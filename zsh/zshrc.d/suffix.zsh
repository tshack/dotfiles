# suffix.zsh
#
#   This is where suffix aliases live.
#
##################################################

# evince
alias -s pdf=evince
alias -s eps=evince

# vlc
alias -s xspf=vlc
alias -s mpv=vlc
alias -s mkv=vlc
alias -s mp4=vlc
alias -s mpg=vlc
alias -s avi=vlc

# less
alias -s log=less

# eye of gnome
alias -s jpg=eog
alias -s png=eog
alias -s gif=eog
alias -s tif=eog
